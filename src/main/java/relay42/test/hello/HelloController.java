package relay42.test.hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;
import java.nio.charset.Charset;
import java.net.*;


@RestController

public class HelloController {

  @Value("${server.port}") private String sport;
  @Value("${azname}") private String azname;
  @Value("${time_app_dns}") private String time_app_dns;
  
  @RequestMapping("/hello")
  public String serverInfo(){
    
    String time="";

    try{
      time = getOurTime();
    }
    catch(Exception ex){
      return "hello " + sport + " " + azname + " " +ex.getMessage();
    }
    return "hello " + sport + " " + azname + " " + time;
  }

  public String getOurTime() throws Exception {

    URL url = new URL("http://"+time_app_dns+"/time");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("GET");
    int status = con.getResponseCode();
    String time="";

    if(status == 200){
      try {
        String response = StreamUtils.copyToString(con.getInputStream() , Charset.forName("UTF-8"));
        if(response.matches("^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{6}Z")){
          time = response;
        }
      }
      catch (Exception ex){
        time = ex.getMessage();
        return time;
      }
     } 
     else{
        time = "notime";
     }

    return time;
  }

}

 


//SimpleClientHttpRequestFactory http = new SimpleClientHttpRequestFactory();
//ClientHttpRequest request = http.createRequest(
//  new URI("http://localhost"), HttpMethod.GET);
//ClientHttpResponse response = request.execute();


