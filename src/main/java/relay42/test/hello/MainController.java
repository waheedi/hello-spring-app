package relay42.test.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController 

public class MainController {

  @RequestMapping("/")
  public String serverPort(){
    return "Welcome to Spring-boot home page";
  }

}